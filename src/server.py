from flask import Flask

server = Flask(__name__)

@server.route("/", methods=['GET'])
def hello():
    return "<h2>This is a message from demo-be</h2>"

if __name__ == "__main__":
    server.run(host='0.0.0.0')
